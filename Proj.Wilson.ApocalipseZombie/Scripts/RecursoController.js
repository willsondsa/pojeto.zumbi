﻿var app = angular.module('resources', []);
var texugo = null;
function convertMode(colection) {
    for (var i = 0; i < colection.length; i++)
    {
        //colection[i].DateRegister = colection[i].DateRegister.toLocaleDateString();
        if(colection[i].Mode=="0"||colection[i].Mode==0)
            colection[i].Mode = "ENTRADA"
        if(colection[i].Mode == "1" || colection[i].Mode == 1)
            colection[i].Mode = "SAÍDA"
        if (colection[i].Mode == "2" || colection[i].Mode == 2)
            colection[i].Mode = "ATUALIZAÇÂO"
    }
}

app.controller('resourceController', function ($scope, $http, recursoService) {
    $scope.tipos = [
       { id: 0, name: 'ENTRADA' },
       { id: 1, name: 'SAIDA' },
       { id: 2, name: 'ALTERADO' }
    ];
    $scope.Inventary = {
        mode: ''
    };
    $scope.resourceData = null;
    $scope.InventaryData = null;
    recursoService.GetAllrecurso().then(function (d) {
        $scope.resourceData = d.data; // Success
    }, function () {
        alert('Error Occured !!!'); // Failed
        window.location.reload();
    });
    $scope.find = function () {     
        var n = $scope.Inventary.mode != null ? $scope.Inventary.mode.name : "";
            $http({
                method: 'Post',
                url: 'api/Inventary/' ,
                params: { name: n }
            }).then(function successCallback(response) {
                $scope.clear();
                if (response.data.length==0)
                    alert("nenhum registro encontrado.");
                $scope.InventaryData = response.data;
                convertMode($scope.InventaryData)
            }, function errorCallback(response) {
                alert("Error : " + response.data.ExceptionMessage);
                window.location.reload();

            });

    };
   $scope.Resource = {
        Id: '',
        Description: '',
        Amount: '',
        Note: '',
       
    };
    $scope.clear = function () {
       $scope.Resource.Id = '';
       $scope.Resource.Description = '';
       $scope.Resource.Amount = '';
       $scope.Resource.Note = '';
    }
    $scope.edit = function (data) {
        $scope.Resource = { Id: data.Id, Description: data.Description, Amount: data.Amount, Note: data.Note }
        $("#save").hide();
        $("#atu").show();
    }
   
    $scope.delete = function (item) {
        $("#save").show();
        $("#atu").hide();
        $http({
            method: 'DELETE',
            url: 'api/Resource/'+item.Id,
        }).then(function successCallback(response) {
            $scope.resourceData.splice(item, 1);
            $scope.clear();
            alert("Recurso deletado com sucesso!!!");
        }, function errorCallback(response) {
            alert("Error : " + response.data.ExceptionMessage);
            window.location.reload();
        });
    };
    //teste
    $scope.update = function () {
        $("#save").show();
        $("#atu").hide();
        if ($scope.Resource.Description != "") {      
            $http({
                method: 'PUT',
                url: 'api/Resource/',
                data:$scope.Resource
            }).then(function successCallback(response) {
                $scope.resourceData = response.data;
                $scope.clear();
                alert("Ok !!!");
                $("#save").show();
                $("#atu").hide()
            }, function errorCallback(response) {
                alert("Error : " + response.data.ExceptionMessage);
            });
        }
        else {
            alert('Preencha  o campo descrição !!');
        }
    };
    $scope.save = function () {
        if ($scope.Resource.Description != "") {
            $http({
                method: 'POST',
                url: 'api/Resource/',
                data: angular.toJson($scope.Resource)
            }).then(function successCallback(response) {
                $scope.clear();
                $scope.resourceData.push(response.data);
            }, function errorCallback(response) {
                alert("Error : " + response.data.ExceptionMessage);
                window.location.reload()
            });
        }
        else {
            alert('Preencha  o campo descrição !!');
        }
    };

});

app.factory('recursoService', function ($http) {
    var fac = {};
    fac.GetAllrecurso = function () {
        return $http.get('api/Resource/');
    }
    return fac;
});

