﻿var app = angular.module('resources', []);

app.controller('resourceController', function ($scope, $http, recursoService) {
    // $scope.user = { name: "fred" }

    $scope.resourceData = null;

    // Fetching records from the factory created at the bottom of the script file
    recursoService.GetAllrecurso().then(function (d) {
        $scope.resourceData = d.data; // Success
    }, function () {
        alert('Error Occured !!!'); // Failed
    });

    $scope.Resource = {
        Id: '',
        Description: '',
        Amount: '',
        Note: '',

    };
    $scope.clear = function () {
        $scope.Resource.Id = '';
        $scope.Resource.Description = '';
        $scope.Resource.Amount = '';
        $scope.Resource.Note = '';
    }
    $scope.edit = function (data) {
        $scope.Resource = { Id: data.Id, Description: data.Description, Amount: data.Amount, Note: data.Note }
    }

    $scope.delete = function (item) {
        $http({
            method: 'DELETE',
            url: 'api/Resource/' + item.Id,
        }).then(function successCallback(response) {
            $scope.resourceData.splice(item, 1);
            alert("Product Deleted Successfully !!!");
        }, function errorCallback(response) {
            alert("Error : " + response.data.ExceptionMessage);
        });
    };
    $scope.update = function () {
        if ($scope.Resource.Description != "") {
            $http({
                method: 'PUT',
                url: 'api/Resource/',
                data: $scope.Resource
            }).then(function successCallback(response) {
                $scope.resourceData = response.data;
                $scope.clear();
                alert("Ok !!!");
                $("#save").show();
                $("#atu").hide()
            }, function errorCallback(response) {
                alert("Error : " + response.data.ExceptionMessage);
            });
        }
        else {
            alert('Please Enter All the Values !!');
        }
    };
    $scope.save = function () {
        if ($scope.Resource.Description != "") {
            $http({
                method: 'POST',
                url: 'api/Resource/',
                data: angular.toJson($scope.Resource)
            }).then(function successCallback(response) {
                $scope.clear();
                //alert("Product Added Successfully !!!");
                $scope.resourceData.push(response.data);
            }, function errorCallback(response) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                alert("Error : " + response.data.ExceptionMessage);
            });
        }
        else {
            alert('Please Enter All the Values !!');
        }
    };

});

app.factory('recursoService', function ($http) {
    var fac = {};
    fac.GetAllrecurso = function () {
        return $http.get('api/Resource/');
    }
    return fac;
});
