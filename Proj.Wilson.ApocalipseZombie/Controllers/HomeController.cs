﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Proj.Wilson.ApocalipseZombie.Models;

namespace Proj.Wilson.ApocalipseZombie.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private Userr users;

        public HomeController()
        {
            users = new Userr();
            if(Base.Users.Count==0)
               System.Web.Security.FormsAuthentication.SignOut();

        }
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Logof()
        {
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("index");
        }
        public ActionResult LogOn()
        {
            return View(users);
        }
        public ActionResult Validar(Userr usu)
        {
            if (ModelState.IsValid)
            {
                var user = users.GetAll().Where(u => u.Login == usu.Login && u.Password == usu.Password).FirstOrDefault();
                if (user != null)
                {
                    System.Web.Security.FormsAuthentication.SetAuthCookie(user.Id, false);

                    return this.RedirectToAction("index");
                }
                else
                {
                    ModelState.AddModelError("error", "Usuario não encontrado");
                    return this.View("LogOn", usu);
                }
            }
            else
                return this.View("LogOn", usu);
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult Save(Userr usu)
        { bool erro = false;
            if (ModelState.IsValid)
            {
                users.GetAll().ToList().ForEach(y =>
                {
                    if (y.Login == usu.Login)
                    {
                        ModelState.AddModelError("Login", "Informe outro Login");
                        erro = true;
                    }
                        
                    if(y.Password==usu.Password)
                    {
                        ModelState.AddModelError("Password", "Informe outro Password");
                        erro = true;
                    }
                        

                });
                if(erro)
                    return this.View("Create", usu);
                else
                {
                    users.Insert(usu);
                    return this.View("LogOn", usu);
                }
                    
            }
            else
                return this.View("Create", usu);
        }
    }
}