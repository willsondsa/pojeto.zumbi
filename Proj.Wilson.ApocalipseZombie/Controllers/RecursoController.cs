﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Proj.Wilson.ApocalipseZombie.Models;

namespace Proj.Wilson.ApocalipseZombie.Controllers
{
    [Authorize]
    public class ResourceController : ApiController
    {
        public Resource recurso;

        public ResourceController()
        {
            recurso = new Resource();
        }
        [Authorize]
        public IList<Resource> Get()
        {
            return recurso.GetAll();
        }
        [Authorize]
        public Resource Post(Resource p)
        {
            recurso.Insert(p, this.User.Identity.Name);
            return p;
        }
        [Authorize]
        public IEnumerable<Resource> Put(Resource p)
        { 
            recurso.Update(p, this.User.Identity.Name);
            return recurso.GetAll();
        }
        [Authorize]
        public void Delete(string id)
        {
            recurso.Delete(recurso.GetAll().Where(r=>r.Id==id).FirstOrDefault(), this.User.Identity.Name);
        }
    }
}
