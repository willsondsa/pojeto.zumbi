﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Proj.Wilson.ApocalipseZombie.Models;

namespace Proj.Wilson.ApocalipseZombie.Controllers
{   
    public class InventaryController : ApiController
    {
        [Authorize]
        public IEnumerable<Audit> Post(string name=null)
        {
            if (!string.IsNullOrEmpty(name))
            {
                Mode m = (Mode)Enum.Parse(typeof(Mode), name);
                return Base.Audits.Where(au => au.Mode == m);
            }
            return Base.Audits;
        }
    }
}
