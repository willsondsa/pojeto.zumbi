﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proj.Wilson.ApocalipseZombie.Models
{
    public class Userr
    {   public Userr()
        {  if(usuarios==null)
            usuarios = new List<Userr>();
        }
        public string Id { get; set; }
        [Display(Name = "Login")]
        [Required(ErrorMessage = "Informe o Login")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Informe a senha do usuário")]
        [DataType(System.ComponentModel.DataAnnotations.DataType.Password)]
        public string Password { get; set; }
        public static List<Userr> usuarios { get; set; }
        public void Insert(Userr usu)
        {
            usu.Id = Guid.NewGuid().ToString();
            Base.Users.Add(usu);
        }
        public void Update(Userr usu)
        {
        }
        public void Delete(Userr usu)
        {
           

        }
        public IList<Userr> GetAll()
        {
            return Base.Users;
        }

    }
}