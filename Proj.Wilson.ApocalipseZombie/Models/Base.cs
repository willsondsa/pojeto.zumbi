﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proj.Wilson.ApocalipseZombie.Models
{
    public static class Base 
    {
        private static IList<Resource> resources;
        private static IList<Userr> users;
        private static IList<Audit> audits;
        public static IList<Resource> Resources
        {
            get
            {   if (resources == null)
                    resources = new List<Resource>();
                return resources;
            }
        }

        public static IList<Userr> Users
        {
            get
            {  if (users == null)
                { 
                      users = new List<Userr>();
                    Userr usu = new Userr() { Login = "admin", Password = "admin", Id = Guid.NewGuid().ToString() };
                    users.Add(usu);
                }

                return users;
            }
            set { users = value; }

        }

        public static IList<Audit> Audits
        {
            get
            {  if (audits == null)
                    audits = new List<Audit>();
                return audits;
            }
        }
    }
}