﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proj.Wilson.ApocalipseZombie.Models
{
    public enum Mode
    {
     ENTRADA,
     SAIDA,
     ALTERADO
    }
    public class Audit
    {
        public Userr Usu { get; set; }
        public Resource Rec { get; set; }
        public DateTime DateRegister { get; set; }
        public Mode Mode { get; set; }
       
    }
}