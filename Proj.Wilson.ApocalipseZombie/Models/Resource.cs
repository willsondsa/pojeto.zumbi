﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proj.Wilson.ApocalipseZombie.Models
{
    public class Resource 
    {

        public Resource()
        {

        }
        public string Id { get; set; }
        public string Description { get; set; }
        public int Amount { get; set; }
        public string Note { get; set; }
        public static List<Audit> Audits { get; set; }
        private void AddAudits(Resource resource,string idUsu,Mode m)
        {
            if (!string.IsNullOrEmpty(idUsu))
            {
                Audit au = new Audit();
                au.DateRegister = DateTime.Parse( DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                Userr usu = Base.Users.Where(u => u.Id == idUsu).FirstOrDefault();
                au.Rec = resource;
                au.Usu = usu;
                au.Mode = m;
                Base.Audits.Add(au);
            }
        }
        public void Insert(Resource recu, string idUsu)
        {
            recu.Id = Guid.NewGuid().ToString();
            this.AddAudits(recu, idUsu,Mode.ENTRADA);
            Base.Resources.Add(recu);
            //  Resourses.Add(recu);

        }
        public void Update(Resource recu, string idUsu)
        {

            this.AddAudits(recu, idUsu,Mode.ALTERADO);
            var up = Base.Resources.Where(r => r.Id == recu.Id).FirstOrDefault();
            up.Description = recu.Description;
            up.Amount = recu.Amount;
            up.Note = recu.Note;
            // Recursos.Add(recu);

        }
        public void Delete(Resource recu, string idUsu)
        {
            this.AddAudits(recu, idUsu,Mode.SAIDA);
            Base.Resources.Remove(recu);

        }
        public IList<Resource> GetAll()
        {
            return Base.Resources;
        }
    }
}