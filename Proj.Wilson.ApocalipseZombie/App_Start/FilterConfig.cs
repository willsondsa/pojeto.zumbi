﻿using System.Web;
using System.Web.Mvc;

namespace Proj.Wilson.ApocalipseZombie
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
